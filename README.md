# pingmonitor

***

## Name
Ping Monitor

## Description
Este script realiza o monitoramento rapidamente de uma rede através de ping e alerta qual dispositivo foi desconectado. No final, mostra todos que desconectaram.

## Installation


## Usage
```
python3 pingmonitor.py 10.0.0.0-254
```

## Support

## Authors and acknowledgment
Paulo W F Santiago

## License
MIT

## Project status
Active
