import subprocess
from time import sleep, strftime
from sys import argv
listA = []
listB = []
listDesconectados = {}
first = True

try:
    param1 = argv[1]
    octs = param1.split('.')
    faixa = octs[3].split('-')
    inicio = int(faixa[0])
    fim = int(faixa[1])
    octs.pop()
    prefixo = '.'.join(octs)

    while True:
        ips = []
        tempo = ""
        for i in range(inicio,fim+1):
            if i == inicio or i == fim:
                tempo += strftime("%H:%M:%S") + "  "
    
            ip = prefixo + "." + str(i)
    
            ping = subprocess.run(["ping", "-c1", "-W0.1", ip],stdout=subprocess.DEVNULL)
            if ping.returncode == 0:
                ips.append(ip)
        print(tempo)
    
        if first:
            listA = ips
            listB = ips
            first = False
        else:
            listB = ips
    
        if listA != listB:
            for ip in listA:
                if ip not in listB:
                    listDesconectados.add(ip)
                    print("Desconectou: ", ip)
    
        if listDesconectados:
            listA = listB
    
        sleep(5)


except IndexError:
    print("Informe a faixa de IPs para monitorar. Exemplo toda a rede: 10.0.0.1-254")

except:
    print("Todos que desconectaram nessa execução: \n", listDesconectados)
